import { ChangeEvent, useContext, useState } from "react";
import reactLogo from "./assets/react.svg";
import "./App.css";
import "./Dabbel.css";
import NavBar from "./components/navbar";
import { Routes, Route, Outlet, Link } from "react-router-dom";
import React from "react";
import axios from "axios";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { createNamedContext } from "./utils/createNamedContext";
import useRequiredContext from "./utils/useRequiredContext";
import { userContext, UserContextProvider } from "./components/usercontext";

export default function App() {
  return (
    <div className="App">
      <ToastContainer />
      <UserContextProvider>
        <Routes>
          <Route path="/" element={<Layout />}>
            <Route index element={<Home />} />
            <Route path="set" element={<Get />} />
            <Route path="about" element={<About />} />
            <Route path="dashboard" element={<Dashboard />} />

            {/* Using path="*"" means "match anything", so this route
                acts like a catch-all for URLs that we don't have explicit
                routes for. */}
            <Route path="*" element={<NoMatch />} />
          </Route>
        </Routes>
      </UserContextProvider>
    </div>
  );
}

function Layout() {
  return (
    <div>
      <NavBar />

      {/* An <Outlet> renders whatever child route is currently active,
          so you can think about this <Outlet> as a placeholder for
          the child routes we defined above. */}
      <Outlet />
    </div>
  );
}
function Get() {
  return (
    <div>
      <h2>Home</h2>
    </div>
  );
}
function Home() {
  const userProp = userContext();
  type Stock = {
    price: string;
    symbol: string;
  };
  const [symbol, setSymbol] = useState("");
  const [price, setPrice] = useState("");
  const [stock, setStock] = useState<Stock>();

  const onChangeSymbol = (e: ChangeEvent<HTMLInputElement>) => {
    setSymbol(e.target.value);
  };

  const onChangePrice = (e: ChangeEvent<HTMLInputElement>) => {
    setPrice(e.target.value);
  };
  const getStockInfo = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();

    axios({
      method: "get",
      url: userProp.urlBackend + "/stock/" + symbol,
      responseType: "json",
    })
      .then(function (response) {
        setStock(response.data);
      })
      .catch((e) => {
        toast.error("error:" + e);
      });
  };

  const putStockInfo = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
   let stock : Stock = {symbol:symbol,price:price}
   let json = JSON.stringify(stock)
   console.info("sending stock: "+ json)
    axios({
      method: "put",
      url: userProp.urlBackend + "/stock",
      headers: {
        'Content-Type': 'application/json'
        },
      data: json,
    })
      .then(function (response) {
        console.log(response.data);
        toast("success");
      })
      .catch((e) => {
        toast.error("error:" + e);
      });
  };

  return (
    <>
      <h2>Home</h2>
      <h3>Stock Info</h3>
      <div className="card">
        <p>symbol: {stock?.symbol}</p>
        <p>price : {stock?.price}</p>
      </div>
      <div className="form-container">
        <form onSubmit={getStockInfo}>
          <fieldset>
          <legend>Get Stock Price</legend>
            <div className="controlset">
              <label className="active">Symbol</label>

              <input
                value={symbol}
                type="text"
                onChange={onChangeSymbol}
                className="validate"
              />

              <div>
                <button className="button" type="submit" name="action">
                  Get Price
                </button>
              </div>
            </div>
          </fieldset>
        </form>
      </div>

      <div className="form-container">
        <form onSubmit={putStockInfo}>
          <fieldset>
          <legend>Set Stock Info</legend>
            <div className="controlset">
              <label className="label">Symbol</label>
              <input
                value={symbol}
                type="text"
                onChange={onChangeSymbol}
                className="validate"
              />
             </div> 
            <div className="controlset">
              <label>Price</label>
              <input
                value={price}
                type="text"
                onChange={onChangePrice}
                className="validate"
              />
              </div> 

            <button className="button" type="submit" name="action">
              Set Price
            </button>
          </fieldset>
        </form>
      </div>
    </>
  );
}

function About() {
  const userProp = userContext();

  const [url, setUrl] = useState("http://localhost:8080");
  const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();

    axios({
      method: "get",
      url: url + "/ping",
      responseType: "text",
    })
      .then(function (response) {
        toast("success!");
        console.info(response.data);
        userProp.urlBackend = url;
      })
      .catch((e) => {
        toast.error("error:" + e);
      });
  };

  const onChangeUrl = (e: ChangeEvent<HTMLInputElement>) => {
    setUrl(e.target.value);
  };

  return (
    <div>
      <h2>Simple React App</h2>
      demo odo tool
      <form onSubmit={handleSubmit}>
        <div className="row">
          <label className="active">Backend URL</label>

          <div className="input-field col s6">
            <input
              value={url}
              id="backend_url"
              type="text"
              onChange={onChangeUrl}
              className="validate"
            />
          </div>
        </div>

        <div className="row">
          <button className="button" type="submit" name="action">
            Update
          </button>
        </div>
      </form>
    </div>
  );
}

function Dashboard() {
  return (
    <div>
      <h2>Dashboard</h2>
    </div>
  );
}

function NoMatch() {
  return (
    <div>
      <h2>Nothing to see here!</h2>
      <p>
        <Link to="/">Go to the home page</Link>
      </p>
    </div>
  );
}
