import React from "react";
import { FunctionComponent } from "react";
import { createNamedContext } from "../utils/createNamedContext";
import useRequiredContext from "../utils/useRequiredContext";

type UserProps = {
    urlBackend: string
   };
  
  const UserContext = createNamedContext<UserProps | undefined>(
    "UserProps",
    undefined
  );
  

  export const userContext = () => useRequiredContext(UserContext);


  export const UserContextProvider: React.FC<{children: React.ReactNode}> = ({ children }) => {
    const [userProp,setUserProps] = React.useState<UserProps>({urlBackend:""});

    const setUrlBackend = (
        u: string,
       ) => {
       setUserProps({urlBackend:u});
      };

      const getUrlBackend = ()=>{return userProp.urlBackend}

    return (
        <UserContext.Provider value={userProp}>
          {children}
        </UserContext.Provider>
      );
}